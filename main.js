(function(window){

    /*
    **-----------------------------------------------------------
    **	js utility for every elements
    **-----------------------------------------------------------
    */
   





    window.JsUtility = new class JsUtility{


        constructor(){

            this.anim = {
                set: (element, option) => {
                    if (this.is_defined(this.anim[option.name])) {
                        this.anim[option.name](element, option);
                    }
                },
    
                fadeout: (element, option) => {
                    option = Object.assign({
                        class: 'my-fade-out',
                        time: 2000,
                        element: element,
                        inview: true,
                    }, option);
                    this.recursive(option, function(option){
                        if (option.inview && !window.JsUtility.isScrolledIntoView(option.element)) {
                            return true;
                        }
                        window.JsUtility.addClass(option.element, option.class);
                        return false;
                    });
                },
    
                fadein: (element, option) => {
                    option = Object.assign({
                        class: 'my-fade-in',
                        time: 2000,
                        element: element,
                        inview: true,
                    }, option);
                    this.recursive(option, function(option){
                        if (option.inview && !window.JsUtility.isScrolledIntoView(option.element)) {
                            return true;
                        }
                        window.JsUtility.addClass(option.element, option.class);
                        return false;
                    });
                },
    
                counter: (element, option) => {
                    option = Object.assign({
                        mode: true,
                        start: 0,
                        target: 0,
                        numer: 0,
                        time: 80,
                        delta: 1,
                        element: element,
                    }, option);
                    option.mode = (option.start < option.target ? true : false);
                    option.delta = (Math.max(option.target, option.start) -  Math.min(option.target, option.start)) / option.time;
                    option.number = parseFloat(option.mode ? option.start : option.start);
                    element.innerHTML = parseInt(option.start);
                    this.recursive(option, function(option){
                        if (window.JsUtility.isScrolledIntoView(option.element)) {
                            let num = parseInt(element.innerHTML);
                            if (option.mode && num < option.target) {
                                option.element.innerHTML = Math.min(Math.ceil(option.number + option.delta), option.target);
                                option.number += option.delta;
                            }
                            else if (!option.mode && num > option.target) {
                                option.element.innerHTML = Math.max(Math.ceil(option.number - option.delta), option.target);
                                option.number -= option.delta;
                            }
                            else  {
                                return false;
                            }
                        }
                        return true;
                    });
                },
    
                scroll: (element, option) => {
                    option = Object.assign({
                        target: null,
                        less: 0,
                        scroll: {},
                        element: element,
                    }, option);
                    if (this.is(option.target, "string")) {
                        option.target = this.getType(document, option.target).offsetTop;
                    }
                    if (this.is(option.less, "string")) {
                        option.less = this.getType(document, option.less).clientHeight;
                    }
                    if (option.target) {
                        option.scroll.top = option.target - option.less;
                        window.scroll(option.scroll);
                    }
                },
            };

            this. ajax = {
                request : function(url, method, data, callback_good, callback_error) {
                    data.method = method;
                    fetch(url, data)
                    .then(r => r.json().then(data => ({ status: r.status, body: data})))
                    .then(function(obj) {
                        if (!window.JsUtility.is_undefined(callback_good)) {
                            callback_good(obj);
                        }
                    })
                    .catch(function(obj) {
                        if (!window.JsUtility.is_undefined(callback_error)) {
                            callback_error(obj);
                        }
                    });
                },
                post: (url, data, callback_good, callback_error) => {
                    this.ajax.request(url, "POST", data, callback_good, callback_error)
                },
                get: (url, data, callback_good, callback_error) => {
                    this.ajax.request(url, "GET", data, callback_good, callback_error)
                },
                update: (url, data, callback_good, callback_error) => {
                    this.ajax.request(url, "UPDATE", data, callback_good, callback_error)
                },
                patch: (url, data, callback_good, callback_error) => {
                    this.ajax.request(url, "PATCH", data, callback_good, callback_error)
                },
                put: (url, data, callback_good, callback_error) => {
                    this.ajax.request(url, "PUT", data, callback_good, callback_error)
                }
            };
        }

        each(query, callback){
            for(let i = 0; i < query.length; i++) {
                callback(query[i]);
            }
            return query;
        }

        getType(element, query) {
            query = query.trim();
            if (query[0] == '#') {
                return element.getElementById(query.substr(1));
            }
            else if (query[0] == '.') {
                return element.getElementsByClassName(query.substr(1));
            }
            else if (query[0] == '@') {
                return element.getElementsByName(query.substr(1));
            }
            else if (query[0] == '^') {
                return element.getElementsByTagName(query.substr(1));
            }
            return null;
        }

        getKey(dict, key) {
            let keys = key.split('.')
            for(let i = 0; i < keys.length; i++) {
                if (this.is_undefined(dict) || this.is_undefined(dict[keys[i]]))
                    return null;
                dict = dict[keys[i]];
            }
            return dict;
        }

        cleanKey(obj) {
            Object.keys(obj).forEach((key) => {
                if (this.is_undefined(obj[key])) {
                    delete obj[key];
                }
            });
            return obj;
        }

        recursive(option, callback) {
            option.callback = callback;
            function func_recursive(option) {
                if (option.callback(option)) {
                    setTimeout(func_recursive, option.time, option);
                }
            }
            func_recursive(option);
        }

        show(element) {
            element.style.display = 'block';
            return element;
        }

        hide(element) {
            element.style.display = 'none';
            return element;
        }

        toggle(element, value) {
            if (value) {
                element.show();
            } else {
                element.hide();
            }
            return element;
        }

        click(element, callback) {
            return this.on(element, 'click', callback);
        }

        on(element, event, callback) {
            element.addEventListener(event, callback)
            return element;        
        }

        remove(element) {
            element.parentNode.removeChild(element);
            return element == null;
        }

        append(parent, child) {
            if (parent && child) {
                if (this.is(child, "string")) {
                    child = this.create({type: 'p', html: child});
                }
                parent.appendChild(child);
            }
        }

        prepend(element) {
            if (element) {
                element.insertBefore(element, element.firstChild);
            }
            return element;
        }


        addClass(element, className) {
            if (element && className) {
                className = className.split(/(\s+)/).filter( e => e.trim().length > 0);
                let i = 0;
                while (i < className.length) {
                    element.classList.add(className[i]);
                    i += 1;
                }
            }
            return element;
        }

        hasClass(element, className) {
            return element.classList.contains(className)
        }

        removeClass(element, className) {
            element.classList.remove(className);
            return element;
        }

        attr(element, name, value) {
            if (this.is_undefined(value)) {
                return element.getAttribute(name);
            } else {
                element.setAttribute(name, value);
            }
            return element;
        }

        data(element, name, value) {
            if (this.is_undefined(value)) {
                return element.getAttribute('data-' + name);
            } else {
                element.setAttribute('data-' + name, value);
            }
            return element;
        }

        removeAttr(element, name) {
            element.removeAttribute(name);
            return element;
        }

        removeData(element, name) {
            return this.removeAttr(element, 'data-' + name);
        }

        removeAttrReg(element, regex) {
            this.each(element.attributes, (attribute) => {
                if (this.is_defined(attribute.name.match(regex))) {
                    this.removeAttr(element, attribute.name);
                }
            });
        }

        removeDataReg(element, regex) {
            this.removeAttrReg(element, "^data-" + regex);
        }

        ready(callback) {
            if (document.readyState != 'loading'){
                callback();
            } else {
                document.addEventListener('DOMContentLoaded', callback);
            }
            return this;            
        }

        html(element, text) {
            if (this.is_undefined(text)){
                return element.innerHTML;
            } else {
                element.innerHTML = text;
                return element;
            }
        }

        is(value, type) {
            if (typeof value == type) {
                return true;
            }
            return false;
        }

        is_undefined(value) {
            if (typeof value ==  'undefined' || value == null)
                return true;
            return false;
        }

        is_defined(value) {
            return !this.is_undefined(value);
        }


        objectsParse(objs, callback) {
            if (objs) {
                this.each(Object.keys(objs), function(key){
                    callback(key, objs[key]);
                });
            }
            return objs
        };

        create(option) {
            let element = document.createElement(option.type);

            this.addClass(element, option.class);
            this.attr(element, 'id', option.id);
            this.objectsParse(option.attr, (key, value) => {
                this.attr(element, key, value);
            });
            this.objectsParse(option.data, (key, value) => {
                this.data(element, key, value);
            });
            if (Array.isArray(option.child)) {
                this.each(option.child, (el) => {
                    this.append(element, el);
                });
            }
            else if (this.is(option.child, "string")) {
                this.append(element, option.child);
            }
            else if (this.is(option.html, "string")) {
                this.html(element, option.html);
            }
            this.objectsParse(option.event, (key, value) => {
                this.on(element, key, value);
            });
            if (this.is_defined(option.anim)) {
                this.anim.set(element, option.anim);
            }
            if (this.is_defined(option.timeout)) {
                setTimeout(option.timeout.callback, option.timeout.time, element);
            }
            if (option.parent) {
                this.append(option.parent, element);
            }
            return element;
        }

        tobool(str){
            switch(str.toLowerCase().trim())
            {
                case 'true':
                case 'yes':
                case '1':
                    return true;
                case 'false':
                case 'no':
                case '0':
                case null:
                    return false;
                default:
                    return Boolean(string);
            }
        }

        isScrolledIntoView(element) {
            let rect = element.getBoundingClientRect();
            return  (rect.top >= 0) && (rect.bottom <= window.innerHeight);
        }

        AllEvent() {
            let EventsTab = [
                "click",
                "mousedown",
                "mouseup",
                "focus",
                "blur",
                "keydown",
                "change",
                "dblclick",
                "mousemove",
                "mouseover",
                "mouseout",
                "mousewheel",
                "keydown",
                "keyup",
                "keypress",
                "textInput",
                "touchstart",
                "touchmove",
                "touchend",
                "touchcancel",
                "resize",
                "scroll",
                "zoom",
                "select",
                "change",
                "submit",
                "reset"
            ]
            return EventsTab;
        }
    }

    /*
    **-----------------------------------------------------------
    **	bulma js 
    **-----------------------------------------------------------
    */

    window.JsBulma = {
        RegisterApp: [],
        register: function(application, mode, callback) {
            if (JsUtility.is_undefined(this.RegisterApp[application])) {
                this.RegisterApp[application] = [];
            }
            this.RegisterApp[application][mode] = callback;
        },

        SubOption: (option) => {
            return Object.assign({
                type: '',
                class: '',
                id: '',
                attr: {},
                data: {},
                html: "",
                child: [],
                event: {},
                parent: null,
                anim: {},
                timeout: {},
            }, option);
        },
    
        create: function(type, option) {
            if (JsUtility.is_defined(this.RegisterApp[type]['create'])) {
                return this.RegisterApp[type]['create'](option);
            }
        },

        update: function(type, option) {
            if (JsUtility.is_defined(this.RegisterApp[type]['update'])) {
                return this.RegisterApp[type]['update'](option);
            }
        },
    }

    /*
    **-----------------------------------------------------------
    **	bulma app
    **-----------------------------------------------------------
    */

        /*
        **-----------------------------------------------------------
        **	notification
        **-----------------------------------------------------------
        */
       window.JsBulma.register('notification', 'create', 
        (option) => {
            option = window.JsBulma.SubOption(option);
            option.class += ' notification ';
            option.type = 'div';
            option.child.unshift(window.JsUtility.create({
                type: 'button',
                class: 'delete',
                data: {
                    close: true,
                },
                event: {
                    click: function(event){
                        this.parentNode.remove();
                    },
                }
            }));
            return window.JsUtility.create(option);
        });

    /*
    **-----------------------------------------------------------
    **	counter create
    **-----------------------------------------------------------
    */
    window.JsBulma.register('counter', 'create', 
    (option) => {
        let counter = {
            target: option.target,
            start: option.start,
            time: option.time,
        }
        let element = JsUtility.create(option.type || 'p', option);
        JsUtility.anim .counter(element, counter);
        return element;
    });

})(window)

/*
**-----------------------------------------------------------
**	init dom elements
**-----------------------------------------------------------
*/
JsUtility.ready(function(){
    /*
    **-----------------------------------------------------------
    **	scroll
    **-----------------------------------------------------------
    */
    JsUtility.each(document.querySelectorAll('[data-animation="scroll"]'), function(element) {
        let option = {
            less: JsUtility.data(element, 'scroll-less'),
            target: JsUtility.data(element, 'scroll-target'),
            scroll: {
                behavior: JsUtility.data(element, "scroll-behavior") || "smooth"
            }
        }
        JsUtility.click(element, () => {
            JsUtility.anim .scroll(element, option);
        });
        JsUtility.removeDataReg(element, 'animation');
        JsUtility.removeDataReg(element, 'scroll');
    });
    /*
    **-----------------------------------------------------------
    **	counter
    **-----------------------------------------------------------
    */
    JsUtility.each(document.querySelectorAll('[data-animation="counter"]'), function(element) {
        let option = {
            target: JsUtility.data(element, 'counter-target') || element.innerHTML || null,
            start: JsUtility.data(element, 'counter-start') || element.innerHTML || null,
            time: JsUtility.data(element, 'counter-time') || 80,
        }
        JsUtility.anim.counter(element, option);
        JsUtility.removeDataReg(element, 'animation');
        JsUtility.removeDataReg(element, 'counter');
    });
    /*
    **-----------------------------------------------------------
    **	fadeout
    **-----------------------------------------------------------
    */
   JsUtility.each(document.querySelectorAll('[data-animation="fadeout"]'), function(element) {
        let option = {
            class: JsUtility.data(element, 'fadeout-' + 'class'),
            time: JsUtility.data(element, 'fadeout-' + 'time'),
            inview: JsUtility.data(element, 'fadeout-' + 'inview'),
        }
        option = JsUtility.cleanKey(option);
        JsUtility.anim.fadeout(element, option);
        JsUtility.removeDataReg(element, 'animation');
        JsUtility.removeDataReg(element, 'fadeout');
    });
});